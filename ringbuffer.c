#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>

#include "config.h"
#include "locks.h"

static int nr_slots = 0;

static enum lock_types lock_type;
static int* ring;
static int front;
static int rear;

void (*enqueue_fn)(int value) = NULL;
int (*dequeue_fn)(void) = NULL;

void enqueue_ringbuffer(int value)
{
	assert(enqueue_fn);
	assert(value >= MIN_VALUE && value < MAX_VALUE);

	enqueue_fn(value);
}

int dequeue_ringbuffer(void)
{
	int value;

	assert(dequeue_fn);

	value = dequeue_fn();
	assert(value >= MIN_VALUE && value < MAX_VALUE);

	return value;
}


/*********************************************************************
 * TODO: Implement using spinlock
 */
struct spinlock spin_lock;

void enqueue_using_spinlock(int value)
{
	do
	{
		acquire_spinlock(&spin_lock);
		if (front == (rear + 1) % nr_slots)
		{
			release_spinlock(&spin_lock);
		}
		else
		{
			break;
		}
	} while(1);
	ring[rear] = value;
	rear = (rear + 1) % nr_slots;
	release_spinlock(&spin_lock);
}

int dequeue_using_spinlock(void)
{
	acquire_spinlock(&spin_lock);
	if (front == rear)
	{
		release_spinlock(&spin_lock);
		return dequeue_using_spinlock();
	}
	int out = ring[front];
	front = (front + 1) % nr_slots;
	release_spinlock(&spin_lock);
	return out;
}

void init_using_spinlock(void)
{
	enqueue_fn = &enqueue_using_spinlock;
	dequeue_fn = &dequeue_using_spinlock;

	init_spinlock(&spin_lock);
}

void fini_using_spinlock(void)
{
}


/*********************************************************************
 * TODO: Implement using mutex
 */
struct mutex mutex_lock;
void enqueue_using_mutex(int value)
{
	do
	{
		acquire_mutex(&mutex_lock);
		if (front == (rear + 1) % nr_slots)
		{
			release_mutex(&mutex_lock);
		}
		else
		{
			break;
		}
	} while(1);
	ring[rear] = value;
	rear = (rear + 1) % nr_slots;
	release_mutex(&mutex_lock);
}

int dequeue_using_mutex(void)
{
	acquire_mutex(&mutex_lock);
	if (front == rear)
	{
		release_mutex(&mutex_lock);
		return dequeue_using_mutex();
	}
	int out = ring[front];
	front = (front + 1) % nr_slots;
	release_mutex(&mutex_lock);
	return out;
}

void init_using_mutex(void)
{
	enqueue_fn = &enqueue_using_mutex;
	dequeue_fn = &dequeue_using_mutex;

	init_mutex(&mutex_lock);
}

void fini_using_mutex(void)
{
}


/*********************************************************************
 * TODO: Implement using semaphore
 */
void enqueue_using_semaphore(int value)
{
}

int dequeue_using_semaphore(void)
{
	return 0;
}

void init_using_semaphore(void)
{
	enqueue_fn = &enqueue_using_semaphore;
	dequeue_fn = &dequeue_using_semaphore;
}

void fini_using_semaphore(void)
{
}


/*********************************************************************
 * Common implementation
 */
int init_ringbuffer(const int _nr_slots_, const enum lock_types _lock_type_)
{
	assert(_nr_slots_ > 0);
	nr_slots = _nr_slots_;

	/* Initialize lock! */
	lock_type = _lock_type_;
	switch (lock_type) {
	case lock_spinlock:
		init_using_spinlock();
		break;
	case lock_mutex:
		init_using_mutex();
		break;
	case lock_semaphore:
		init_using_semaphore();
		break;
	}

	/* TODO: Initialize your ringbuffer and synchronization mechanism */
	ring = (int*)malloc(sizeof(int) * nr_slots);
	front = 0;
	rear = 0;
	return 0;
}

void fini_ringbuffer(void)
{
	/* TODO: Clean up what you allocated */
	switch (lock_type) {
	default:
		break;
	}

	free(ring);
}
